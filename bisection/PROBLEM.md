# Bisection method #

## Aim ##
Implement the Bisection algorithm in python.

## Background ##
The Bisection method is a very simple root finding algorithm. The root is found
by successivly halving the search space for the root untill the root is found.

## The algorithm ##
 1. choose two initial end points a and b such that f(a) and f(b) have the opposite
 sign. This ensures that the root is inbetween a and b.
 2. Estimate the new root c with

 $$
 c = \frac{a + b}{2}
 $$

 3. Calculate the function value at the midpoint, f( c).
  - If the value of f( c) is sufficiently close to 0 then Bisection has converged
 and *c* is the root of the function.
  - If Bisection has not converged then, replace the value of a with c it the root
  is inbetween c and b, or replace the value of b with c if the root lies inbetween
  a and c.

 4. Loop back to step 2 untill converged.

##  Pseudocode ##

The peudocode for bisection according to wikipedia.

  ~~~
  INPUT: Function f, endpoint values a, b, tolerance TOL, maximum iterations NMAX
  CONDITIONS: a < b, either f(a) < 0 and f(b) > 0 or f(a) > 0 and f(b) < 0
  OUTPUT: value which differs from a root of f(x)=0 by less than TOL

  N ← 1
  While N ≤ NMAX # limit iterations to prevent infinite loop
    c ← (a + b)/2 # new midpoint
    If f(c) = 0 or (b – a)/2 < TOL then # solution found
      Output(c)
      Stop
    EndIf
    N ← N + 1 # increment step counter
    If sign(f(c)) = sign(f(a)) then a ← c else b ← c # new interval
  EndWhile
  Output("Method failed.") # max number of steps exceeded
  ~~~

## Assignment ##

 1. Implement the bisection method in python
 2. Find the roots of the following equations

    i.
    $$ x^2 - 5x -7=0 $$

    ii.

    $$ x^3 + 8x =0 $$

## Refrences ##

https://en.wikipedia.org/wiki/Bisection_method
