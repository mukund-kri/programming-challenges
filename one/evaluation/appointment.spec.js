'use strict';


var expect = require('chai').expect;

var Appointment = require('../solutions/appointment');


describe('Assert', function() {

  // 1st appointment > 2nd appointment
  it('should not over lap 1', function() {
    var appointment1 = new Appointment(
      new Date(2016, 08, 10, 10, 45),  // 10:45 AM 10th Aug 2016
      new Date(2016, 08, 10, 11, 45)   // 11:45 AM 10th Aug 2016
    );

    var appointment2 = new Appointment(
      new Date(2016, 08, 10, 12, 0),  // 11 AM 10th Aug 2016
      new Date(2016, 08, 10, 13, 0)   // 12 noon 10th Aug 2016
    );

    var result = appointment1.clashesWith(appointment2); // Should be false
    expect(result).equals(false);
  });

  // 1st appointment < 2nd appointment
  it('should not over lap 2', function() {

    var appointment1 = new Appointment(
      new Date(2016, 08, 10, 12, 0),  // 11 AM 10th Aug 2016
      new Date(2016, 08, 10, 13, 0)   // 12 noon 10th Aug 2016
    );

    var appointment2 = new Appointment(
      new Date(2016, 08, 10, 10, 45),  // 10:45 AM 10th Aug 2016
      new Date(2016, 08, 10, 11, 45)   // 11:45 AM 10th Aug 2016
    );

    var result = appointment1.clashesWith(appointment2); // Should be false
    expect(result).equals(false);
  });

  // Over lap 1
  it('should over lap 1', function() {
    var appointment1 = new Appointment(
      new Date(2016, 08, 10, 10, 45),  // 10:45 AM 10th Aug 2016
      new Date(2016, 08, 10, 11, 45)   // 11:45 AM 10th Aug 2016
    );

    var appointment2 = new Appointment(
      new Date(2016, 08, 10, 11, 0),  // 11 AM 10th Aug 2016
      new Date(2016, 08, 10, 13, 0)   // 12 noon 10th Aug 2016
    );

    var result = appointment1.clashesWith(appointment2); // Should be false
    expect(result).equals(true);
  });

  // Over lap 2
  it('should over lap 2', function() {
    var appointment1 = new Appointment(
      new Date(2016, 08, 10, 10, 45),  // 10:45 AM 10th Aug 2016
      new Date(2016, 08, 10, 11, 45)   // 11:45 AM 10th Aug 2016
    );

    var appointment2 = new Appointment(
      new Date(2016, 08, 10, 10, 0),  // 11 AM 10th Aug 2016
      new Date(2016, 08, 10, 11, 0)   // 12 noon 10th Aug 2016
    );

    var result = appointment1.clashesWith(appointment2); // Should be false
    expect(result).equals(true);
  });


  // Over lap 3
  it('should over lap 3', function() {
    var appointment1 = new Appointment(
      new Date(2016, 08, 10, 9, 0),  // 10:45 AM 10th Aug 2016
      new Date(2016, 08, 10, 13, 0)   // 11:45 AM 10th Aug 2016
    );

    var appointment2 = new Appointment(
      new Date(2016, 08, 10, 10, 0),  // 11 AM 10th Aug 2016
      new Date(2016, 08, 10, 11, 0)   // 12 noon 10th Aug 2016
    );

    var result = appointment1.clashesWith(appointment2); // Should be false
    expect(result).equals(true);
  });

  // Over lap 3
  it('should over lap 4', function() {
    var appointment2 = new Appointment(
      new Date(2016, 08, 10, 9, 0),  // 10:45 AM 10th Aug 2016
      new Date(2016, 08, 10, 13, 0)   // 11:45 AM 10th Aug 2016
    );

    var appointment1 = new Appointment(
      new Date(2016, 08, 10, 10, 0),  // 11 AM 10th Aug 2016
      new Date(2016, 08, 10, 11, 0)   // 12 noon 10th Aug 2016
    );

    var result = appointment1.clashesWith(appointment2); // Should be false
    expect(result).equals(true);
  });

})
