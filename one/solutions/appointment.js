'use strict';


function Appointment(startTime, endTime) {
  this._startTime = startTime;
  this._endTime = endTime;
};


Appointment.prototype.clashesWith = function(otherAppointment) {
  // write your clash detection code here

  if (otherAppointment._startTime > this._startTime
   && otherAppointment._startTime < this._endTime) {
      return true;
    }
  if (otherAppointment._endTime > this._startTime
   && otherAppointment._endTime < this._endTime) return true;

  if (this._startTime > otherAppointment._startTime
   && this._startTime < otherAppointment._endTime) return true;

  if (this._endTime > otherAppointment._startTime
   && this._endTime < otherAppointment._endTime) return true;

  return false;
};
module.exports = Appointment;
